class TimeUtil {
  static String formatDateTime(DateTime time) => '${time.hour}:${time.minute}';
}
