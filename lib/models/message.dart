import 'package:flutter/foundation.dart';

class Message {
  const Message({
    @required this.text,
    @required this.time,
    this.isBot = false,
  });

  final String text;
  final DateTime time;
  final bool isBot;
}
