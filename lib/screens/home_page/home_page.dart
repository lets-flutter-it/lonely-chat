import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lonelychat/blocs/messages_bloc.dart';
import 'package:lonelychat/blocs/messages_state.dart';
import 'package:lonelychat/screens/home_page/chat_text_field.dart';

import 'chat_list/chat_list.dart';

class HomePage extends StatelessWidget {
  HomePage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lonely Chat')),
      body: BlocBuilder<MessagesBloc, MessagesState>(
        builder: (_, state) => Column(
          children: [
            if (state is NoMessages)
              Expanded(
                child: Center(
                  child: Text('No Messages'),
                ),
              ),
            if (state is MessagesExist)
              Expanded(child: ChatList(state.messages)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ChatTextField(),
            ),
          ],
        ),
      ),
    );
  }
}
