import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:lonelychat/models/message.dart';
import 'package:lonelychat/util/time_util.dart';

import 'chat_avatar.dart';

class ChatBubble extends StatelessWidget {
  const ChatBubble(this.message);

  final Message message;

  @override
  Widget build(BuildContext context) {
    final isBot = message.isBot;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          if (isBot) ChatAvatar(true),
          Expanded(
            child: Column(
              crossAxisAlignment:
                  isBot ? CrossAxisAlignment.start : CrossAxisAlignment.end,
              children: [
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: isBot
                          ? [
                              Colors.blueGrey.shade600,
                              Colors.blueGrey.shade400,
                            ]
                          : [
                              Colors.blue.shade400,
                              Colors.blue.shade800,
                            ],
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      bottomLeft: Radius.circular(isBot ? 0 : 15),
                      bottomRight: Radius.circular(isBot ? 15 : 0),
                    ),
                  ),
                  padding: EdgeInsets.all(6.5),
                  child: Text(
                    message.text,
                    style: TextStyle(fontSize: 17, color: Colors.white),
                  ),
                ),
                Text(TimeUtil.formatDateTime(message.time)),
              ],
            ),
          ),
          if (!isBot) ChatAvatar(false),
        ],
      ),
    );
  }
}
