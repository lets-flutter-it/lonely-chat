import 'package:flutter/material.dart';

class ChatAvatar extends StatelessWidget {
  const ChatAvatar(this.isBot);

  final bool isBot;

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.only(
          bottom: 15,
          left: isBot ? 0 : 8,
          right: isBot ? 8 : 0,
        ),
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(
                'assets/${isBot ? 'bot.png' : 'lonelyGuy.jpg'}',
              ),
            ),
          ),
          height: 35,
          width: 35,
        ),
      );
}
