import 'package:flutter/material.dart';
import 'package:lonelychat/models/message.dart';

import 'chat_bubble.dart';

class ChatList extends StatelessWidget {
  const ChatList(this.messages);

  final List<Message> messages;

  @override
  Widget build(BuildContext context) => ListView(
        padding: EdgeInsets.all(4),
        reverse: true,
        children: [
          ...messages.map((msg) => ChatBubble(msg)).toList().reversed,
        ],
      );
}
