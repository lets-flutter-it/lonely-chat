import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lonelychat/blocs/messages_bloc.dart';
import 'package:lonelychat/blocs/messages_event.dart';

class ChatTextField extends StatelessWidget {
  const ChatTextField();

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<MessagesBloc>(context);

    return TextField(
      controller: bloc.controller,
      decoration: InputDecoration(
        suffixIcon: IconButton(
          icon: Icon(Icons.send),
          onPressed: () => bloc.add(SendMessage()),
        ),
        icon: Container(
          alignment: Alignment.centerRight,
          width: 15,
          child: IconButton(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.zero,
            icon: Icon(FontAwesomeIcons.camera),
            onPressed: () {},
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        hintText: 'Type your message Mr. Lonely...',
      ),
    );
  }
}
