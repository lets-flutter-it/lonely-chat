import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lonelychat/blocs/messages_bloc.dart';
import 'package:lonelychat/screens/home_page/home_page.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: BlocProvider(
          create: (_) => MessagesBloc(),
          child: HomePage(),
        ),
      );
}
