import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lonelychat/blocs/messages_event.dart';
import 'package:lonelychat/blocs/messages_state.dart';
import 'package:lonelychat/models/message.dart';

class MessagesBloc extends Bloc<MessagesEvent, MessagesState> {
  final controller = TextEditingController();

  @override
  MessagesState get initialState => NoMessages();
  final random = Random();

  @override
  Stream<MessagesState> mapEventToState(MessagesEvent event) async* {
    if (event is SendMessage) {
      final text = controller.text;
      final message = Message(text: text, time: DateTime.now());
      controller.clear();
      yield MessagesExist(state.messages..add(message));
      final secondsToWait = random.nextInt(10) + 1;
      await Future.delayed(Duration(seconds: secondsToWait));
      final reversedText = text.split('').reversed.join();
      yield MessagesExist(
        state.messages
          ..add(
            Message(
              text: reversedText,
              isBot: true,
              time: DateTime.now(),
            ),
          ),
      );
    }
  }

  @override
  Future<Function> close() async {
    super.close();
    controller?.dispose();
  }
}
