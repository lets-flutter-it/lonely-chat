import 'package:lonelychat/models/message.dart';

abstract class MessagesState {
  final List<Message> messages = [];
}

class NoMessages extends MessagesState {}

class MessagesExist extends MessagesState {
  MessagesExist(this.messages);

  @override
  final List<Message> messages;
}
